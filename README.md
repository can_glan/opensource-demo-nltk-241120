I. Introduction

This project demonstrates how to set up and run a text processing application using Natural Language Toolkit (NLTK) on a Huawei Cloud Elastic Cloud Server (ECS). The application is designed to perform tokenization and part-of-speech (POS) tagging, and it provides a user-friendly web interface built with Gradio.
﻿

II. Configuration and Code Execution on Kunpeng CPU Environment

Basic Configuration of Huawei Cloud Elastic Cloud Server (ECS)

Instance Specification: It is recommended to choose Kunpeng general-purpose computing enhanced type kc1.large.4 (equipped with 2vCPUs and 8GiB of memory).

Operating System: Huawei Cloud EulerOS 2.0 64bit for kAi2p with HDK 23.0.1 and CANN 7.0.0.1 RC.

Configure other settings as needed (e.g., network, security group).

﻿
Create a new Conda environment with Python 3.9.

```
conda create --name nltk python=3.9
conda activate nltk
```


Clone the NLTK repository from GitHub.

```
git clone https://github.com/nltk/nltk
cd nltk
pip install -r requirements-ci.txt
```


run the run-pos.py, the script will download the necessary NLTK resources and perform tokenization and POS tagging.
```
python run-pos.py
```

To keep the Gradio application running in the background, use the nohup command.
```
pip install gradio
nohup python gradio_show.py &
```

Open a web browser and navigate to the following URL:

http://<ECS_Public_IP>:7862

﻿
III. Related Links

NLTK Official Project Address: https://github.com/nltk/nltk

Detailed Steps of This Project: [CSDN Blog Link](http://blog.csdn.net/qq_54958500/article/details/143701953?sharetype=blogdetail&sharerId=143701953&sharerefer=PC&sharesource=qq_54958500&spm=1011.2480.3001.8118)

Summary of This Project's Experience: [Huawei Cloud Blog Link](https://bbs.huaweicloud.com/blogs/439886)

﻿
IV. Acknowledgements

Special thanks to the NLTK team and the Kunpeng processor for their contributions to this project.
