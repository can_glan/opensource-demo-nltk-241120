import nltk
from nltk.tokenize import word_tokenize
from nltk import pos_tag
from nltk.downloader import Downloader
import os
 
# 设置 NLTK 数据路径
data_path = './nltk_data'
os.makedirs(data_path, exist_ok=True)
nltk.data.path.append(data_path)
 
# 下载所需的资源包
if not os.path.exists(os.path.join(data_path, 'tokenizers', 'punkt_tab')):
    # 创建 Downloader 对象，并指定下载目录
    downloader = Downloader(download_dir=data_path)
    # 下载 punkt_tab 资源包
    downloader.download('punkt_tab')
if not os.path.exists(os.path.join(data_path, 'taggers', 'averaged_perceptron_tagger_eng')):
    # 创建 Downloader 对象，并指定下载目录
    downloader = Downloader(download_dir=data_path)
    # 下载 averaged_perceptron_tagger_eng 资源包
    downloader.download('averaged_perceptron_tagger_eng')
 
print("start_pos")
# 示例文本
text = "I really like the new design of your website!"
 
# 分词
tokens = word_tokenize(text)
 
# 词性标注
pos_tags = pos_tag(tokens)
 
print("Tokenized and Tagged Text:", pos_tags)